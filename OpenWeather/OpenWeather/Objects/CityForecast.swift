//
//  CityForecast.swift
//  OpenWeather
//
//  Created by Alvin Luu on 8/6/18.
//  Copyright © 2018 None. All rights reserved.
//

import Foundation

class CityForecast: NSObject {
    var timestamp: Int64!
    var weather: CityWeather!
    
    override init() {
        weather = CityWeather()
    }
}
