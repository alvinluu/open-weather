//
//  Venue.swift
//  OpenWeather
//
//  Created by Alvin Luu on 8/5/18.
//  Copyright © 2018 None. All rights reserved.
//

import MapKit
import AddressBook

class Venue: NSObject, MKAnnotation {
    let title: String?
    let locationName: String?
    let coordinate: CLLocationCoordinate2D
    
    init(title: String, locationName: String?, coordinate: CLLocationCoordinate2D) {
        self.title = title
        self.locationName = locationName
        self.coordinate = coordinate
        
        super.init()
    }
    
    var subtitle: String? {
        return locationName
    }
}
