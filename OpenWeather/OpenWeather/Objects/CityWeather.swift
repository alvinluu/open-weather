//
//  CityWeather.swift
//  OpenWeather
//
//  Created by Alvin Luu on 8/4/18.
//  Copyright © 2018 None. All rights reserved.
//

import Foundation


class CityWeather: NSObject {
    var city: String = ""
    var temperature: Int?
    var humidity: Int?
    var windSpeed: Int?
    var chanceOfRain: Int?
    var weatherDesc: String = ""
    var datetimeString: String!
    var country: String = ""
    var latitude: Double!
    var longitude: Double!
    
    //MARK: Render text functions
    var temperatureText:String {
        get {
            return getTemperatureText(value: temperature ?? 0)
        }
    }
    var humidityText:String {
        get {
            return getPercentageText(value: humidity ?? 0)
        }
    }
    var windSpeedText:String {
        get {
            return getSpeedText(value: windSpeed ?? 0)
        }
    }
    var chanceOfRainText:String {
        get {
            return getPercentageText(value: chanceOfRain ?? 0)
        }
    }
    var datetimeText:String {
        get {
            let formatter = DateFormatter()
            // example "dt_txt": "2018-08-11 00:00:00"
            formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            
            let yourDate = formatter.date(from: datetimeString)
            formatter.dateFormat = "MM/dd"
            
            let myStringafd = formatter.string(from: yourDate!)
            
            return myStringafd
        }
    }
    
    var cityCountryText:String {
        return "\(city), \(country)"
    }
    
    
    //MARK: Concatenate String for render
    private func getTemperatureText(value: Int) -> String {
        return "\(value)º"
    }
    private func getPercentageText(value: Int) -> String {
        return "\(value)%"
    }
    private func getSpeedText(value: Int) -> String  {
        return "\(value) mph"
    }
    
    //MARK: Parse
    
    func parse(json: [String:Any]) {
        if let main = json["main"] as? [String:Any] {
            if let temp = main["temp"] as? NSNumber {
                self.temperature = temp.intValue
            }
            if let humidity = main["humidity"] as? Int {
                self.humidity = humidity
            }
            
        }
        if let wind = json["wind"] as? [String:Any] {
            if let speed = wind["speed"] as? NSNumber {
                self.windSpeed = speed.intValue
            }
        }
        if let cityName = json["name"] as? String {
            self.city = cityName
        }
        
        if let weatherArray = json["weather"] as? [Dictionary<String,Any>],
            let weather = weatherArray.first {
            if let desc = weather["description"] as? String
            {
                self.weatherDesc = desc
            }
            
        }
        
        if let dt = json["dt_txt"] as? String {
            self.datetimeString = dt
        }
        
        
        if let coord = json["coord"] as? [String:Any],
            let lat = coord["lat"] as? Double,
            let lon = coord["lon"] as? Double {
            self.latitude = lat
            self.longitude = lon
        }
        
        if let sys = json["sys"] as? [String:Any],
            let country = sys["country"] as? String {
            self.country = country
        }
        
    }
}
