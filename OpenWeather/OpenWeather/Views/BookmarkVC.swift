//
//  BookmarkViewController.swift
//  OpenWeather
//
//  Created by Alvin Luu on 8/4/18.
//  Copyright © 2018 None. All rights reserved.
//

import UIKit
import MapKit
import CoreData

class BookmarkVC: BaseViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    var cities:[WeatherStatus]!
    
    // MARK: Views cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = "Bookmark"
        self.navigationController?.navigationBar.backgroundColor = kAPP_STYLE_COLOR
        
        addSlideMenuButton()
    }
    override func viewWillAppear(_ animated: Bool) {
        
        cities = getCityWeathers()
        tableView.reloadData()
        editTableViewRows(firstTime: true)
        
        super.viewWillAppear(animated)
    }
    
    // MARK: private functions
    @objc func launchHelpScreen() {
        
        self.performSegue(withIdentifier: "launchHelpVC", sender: nil)
    }
    @objc func launchSettingScreen() {
        
        
        self.performSegue(withIdentifier: "launchSettingsVC", sender: nil)
    }
    
    @objc func addBookmark() {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "MapVC") as! MapVC
        
        
        navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func editTableViewRows(firstTime:Bool) {
        
        if !firstTime {
            self.tableView.isEditing = !self.tableView.isEditing
        }
        let editSystemItem = isEditing ? UIBarButtonSystemItem.save : .edit
        
        let editBtn = UIBarButtonItem(barButtonSystemItem: editSystemItem, target: self, action: #selector(editTableViewRows))
        let addBtn = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(addBookmark))
        
        self.navigationItem.setRightBarButtonItems([addBtn, editBtn], animated: true)
        self.tableView.reloadData()
    }
    
    // MARK: - Table view data source
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cities.count
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "CityStatusVC") as! CityStatusVC
        
        vc.weatherStatus = cities[indexPath.row]
        
        navigationController?.pushViewController(vc, animated: true)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        
        let w = cities[indexPath.row]
        
        cell.textLabel?.text = w.name
        
        
        return cell
    }
    
    
    // MARK: - Core Data
    func getCityWeathers() -> [WeatherStatus] {
        
        var result = [WeatherStatus]()
        
        // Create Managed Object
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return result
        }
        let managedObjectContext = appDelegate.persistentContainer.viewContext
        
        // Initialize Fetch Request
        let fetchRequest:NSFetchRequest<WeatherStatus> = WeatherStatus.fetchRequest()
        
        
        do {
            result = try managedObjectContext.fetch(fetchRequest)
            print(result)
            return result
            
        } catch {
            let fetchError = error as NSError
            print(fetchError)
        }
        return result
        
    }
}
