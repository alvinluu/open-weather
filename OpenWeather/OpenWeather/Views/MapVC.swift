//
//  MapvC.swift
//  OpenWeather
//
//  Created by Alvin Luu on 8/4/18.
//  Copyright © 2018 None. All rights reserved.
//

import UIKit
import MapKit
import CoreData

class MapVC: UIViewController, UIGestureRecognizerDelegate {
    
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var tableView: UITableView!
    var searchBar:UISearchBar!
    var searchResults:[CityWeather] = [CityWeather]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        mapView.delegate = self
        self.navigationController?.navigationBar.backgroundColor = kAPP_STYLE_COLOR
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let screenSize = UIScreen.main.bounds.size
        searchBar = UISearchBar(frame: CGRect(x: 0, y: 0, width: screenSize.width-90, height: 40))
        searchBar.autoresizingMask = UIViewAutoresizing.flexibleWidth
        searchBar.placeholder = "Enter City"
        searchBar.showsSearchResultsButton = true
        searchBar.barStyle = .default
        searchBar.searchBarStyle = .prominent
        searchBar.returnKeyType = .search
        searchBar.delegate = self
        
        let leftNavBarButton = UIBarButtonItem(customView:searchBar)
        let searchButton = UIBarButtonItem(barButtonSystemItem: .search, target: self, action: #selector(tapSearch))
        
        self.navigationItem.leftItemsSupplementBackButton = true
        self.navigationItem.leftBarButtonItems = [leftNavBarButton]
        self.navigationItem.setRightBarButton(searchButton, animated: true)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        checkLocationServiceAuthentactionStatus()
        
        let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(handleTap(gestureReconizer:)))
        gestureRecognizer.delegate = self
        mapView.addGestureRecognizer(gestureRecognizer)
        
    }
    
    
    var currentAnnotation: MKPointAnnotation!
    
    private let regionRadius: CLLocationDistance = 1000
    func zoomMapOn(location: CLLocation) {
        let coordinateRegion = MKCoordinateRegionMakeWithDistance(location.coordinate, regionRadius * 2.0, regionRadius * 2.0)
        mapView.setRegion(coordinateRegion, animated: true)
        
        
    }
    
    func getCoordinateText(coordinate: CLLocationCoordinate2D) -> String {
        return "\(Double(round(1000*coordinate.latitude)/1000))ºN, \(Double(round(1000*coordinate.longitude)/1000))ºW"
    }
    
    // MARK: - Alert
    
    func getPermissionToAddLocationToBookmark() {
        let alertController = UIAlertController(title: "Add Location", message: "Do you want to add city, \(weather.name!), to Bookmark?", preferredStyle: .alert)
        
        let action1 = UIAlertAction(title: "Add", style: .default) { (action:UIAlertAction) in
            print("You've pressed add");
            
            // Save to CoreData
            self.weather.add(completion: { 
                // return to BookmarkVC
                
            })
            
            
            self.navigationController?.popViewController(animated: true)
            
        }
        
        let action2 = UIAlertAction(title: "No", style: .cancel) { (action:UIAlertAction) in
            print("You've pressed cancel");
            // delete annotation
            self.mapView.removeAnnotation(self.currentAnnotation)
            
            // stay in MapVC
            
        }
        
        alertController.addAction(action1)
        alertController.addAction(action2)
        self.present(alertController, animated: true, completion: nil)
    }
    
    // MARK: - HTTP Request
    var weather:WeatherStatus!
    
    func HTTP_GET_Find_Request(city:String){
        
        // example url http://api.openweathermap.org/data/2.5/find?q=Bosto&appid=c6e381d8c7ff98f0fee43775817cf6ad&type=like
        let Url = String(format: URL_FETCH_CITY)
        let parameterDictionary = ["q" : city, "appid" : "c6e381d8c7ff98f0fee43775817cf6ad", "type" : "like"] as [String : Any]
        var components = URLComponents()
        components.queryItems = queryItems(dictionary: parameterDictionary)
        let parameterString = components.url?.description ?? ""
        
        
        guard let serviceUrl = URL(string: Url + parameterString) else { return }
        var request = URLRequest(url: serviceUrl)
        request.httpMethod = "GET"
        
        let session = URLSession.shared
        session.dataTask(with: request) { (data, response, error) in
            if let response = response {
                print(response)
            }
            if let data = data {
                do {
                    let json = try JSONSerialization.jsonObject(with: data, options: []) as! [String : Any]
                    print("json data")
                    print(json)
                    
                    
                    if let list = json["list"] as? [[String:Any]] {
                        
                        for dict:[String:Any] in list {
                            let weather = CityWeather()
                            weather.parse(json: dict)
                            
                            self.searchResults.append(weather)
                        }
                        
                        DispatchQueue.main.async {
                            self.tableView.isHidden = false
                            self.tableView.reloadData()
                        }
                    }
                    
                } catch {
                    print(error)
                }
            }
            }.resume()
    }
    
    func HTTP_GET_Request(coordinate:CLLocationCoordinate2D){
        
        // example url http://api.openweathermap.org/data/2.5/weather?lat=0&lon=0&appid=c6e381d8c7ff98f0fee43775817cf6ad&units=metric
        //        let Url = String(format: "http://api.openweathermap.org/data/2.5/weather?lat=0&lon=0&appid=c6e381d8c7ff98f0fee43775817cf6ad&units=metric")
        let Url = String(format: URL_FETCH_WEATHER)
        let parameterDictionary = ["lat" : coordinate.latitude, "lon" : coordinate.longitude, "appid" : "c6e381d8c7ff98f0fee43775817cf6ad", "units" : SettingsVC().unit_format] as [String : Any]
        var components = URLComponents()
        components.queryItems = queryItems(dictionary: parameterDictionary)
        let parameterString = components.url?.description ?? ""
        
        
        guard let serviceUrl = URL(string: Url + parameterString) else { return }
        var request = URLRequest(url: serviceUrl)
        request.httpMethod = "GET"
        
        let session = URLSession.shared
        session.dataTask(with: request) { (data, response, error) in
            if let response = response {
                print(response)
            }
            if let data = data {
                do {
                    let json = try JSONSerialization.jsonObject(with: data, options: []) as! [String : Any]
                    print("json data")
                    print(json)
                    
                    DispatchQueue.main.async {
                        // Create Managed Object
                        guard let appDelegate =
                            UIApplication.shared.delegate as? AppDelegate else {
                                return
                        }
                        let managedObjectContext = appDelegate.persistentContainer.viewContext
                        let entityDescription = NSEntityDescription.entity(forEntityName: "WeatherStatus", in: managedObjectContext)
                        let w = NSManagedObject(entity: entityDescription!, insertInto: managedObjectContext) as! WeatherStatus
                        
                        
                        w.parse(json: json)
                        self.weather = w
                        self.getPermissionToAddLocationToBookmark()

                    }
                    
                }catch {
                    print(error)
                }
            }
            }.resume()
    }
    
    func queryItems(dictionary: [String:Any]) -> [URLQueryItem] {
        return dictionary.map {
            // Swift 3
            URLQueryItem(name: $0, value: "\($1)")
            
            // Swift 4
            //            URLQueryItem(name: $0.0, value: $0.1)
        }
    }
    // MARK: - Navigator
    func tapSearch() {
        
        if let searchText = searchBar.text
        {
            print("Search", searchText)
            HTTP_GET_Find_Request(city: searchText)
        }
    }
    
    func tapBack() {
        navigationController?.popViewController(animated: true)
    }
    
    // MARK: - Gesture
    func handleTap(gestureReconizer: UILongPressGestureRecognizer) {
        let location = gestureReconizer.location(in: mapView)
        let coordinate = mapView.convert(location, toCoordinateFrom: mapView)
        
        //Add annotation
        currentAnnotation = MKPointAnnotation()
        currentAnnotation.coordinate = coordinate
        currentAnnotation.title = getCoordinateText(coordinate: coordinate)
        mapView.addAnnotation(currentAnnotation)
        
        //getPermissionToAddLocationToBookmark(annotation: annotation, coordinate: coordinate)
        HTTP_GET_Request(coordinate: coordinate)
        
    }
    
    // MARK: - Current location
    
    var locationManager = CLLocationManager()
    
    func checkLocationServiceAuthentactionStatus()
    {
        locationManager.delegate = self
        if CLLocationManager.authorizationStatus() == .authorizedWhenInUse {
            mapView.showsUserLocation = true
            locationManager.startUpdatingLocation()
        } else {
            locationManager.requestWhenInUseAuthorization()
            locationManager.startUpdatingLocation()
        }
        
        if let location = locationManager.location {
            zoomMapOn(location: location)
        }
        
    }
    
    
}

// MARK: CLLocationManagerDelegate
extension MapVC : CLLocationManagerDelegate
{
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        self.mapView.showsUserLocation = true
    }
}

// MARK: MKMapViewDelegate
extension MapVC : MKMapViewDelegate
{
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        if let annotation = annotation as? Venue {
            let identifier = "pin"
            var view: MKPinAnnotationView
            if let dequeuedView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier) as? MKPinAnnotationView {
                dequeuedView.annotation = annotation
                view = dequeuedView
            } else {
                view = MKPinAnnotationView(annotation: annotation, reuseIdentifier: identifier)
                view.canShowCallout = true
                view.calloutOffset = CGPoint(x: -5, y: 5)
                view.rightCalloutAccessoryView = UIButton(type: .detailDisclosure) as UIView
                
            }
            return view
        }
        
        return nil
    }
    
}
// MARK: UITableViewDelegate
extension MapVC : UITableViewDelegate, UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return searchResults.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell")
        
        let w = searchResults[indexPath.row]
        let text = w.cityCountryText
        cell?.textLabel?.text = text
        
        return cell!
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let w = searchResults[indexPath.row]
        
        let coord = CLLocationCoordinate2D(latitude: w.latitude, longitude: w.longitude)
        let location = CLLocation(latitude: coord.latitude, longitude: coord.longitude)
        
        zoomMapOn(location: location)
        
        //Add annotation
        currentAnnotation = MKPointAnnotation()
        currentAnnotation.coordinate = coord
        currentAnnotation.title = w.cityCountryText
        mapView.addAnnotation(currentAnnotation)
        
        HTTP_GET_Request(coordinate: coord)
    }
}
// MARK: UISearchBarDelegate
extension MapVC : UISearchBarDelegate
{
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        tapSearch()
    }
    
    func searchBarResultsListButtonClicked(_ searchBar: UISearchBar) {
        searchBar.text = ""
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchBar.text?.isEmpty == true {
            tableView.isHidden = true
        }
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.text = ""
    }
}
