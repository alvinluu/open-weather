//
//  HelpVC.swift
//  OpenWeather
//
//  Created by Alvin Luu on 8/6/18.
//  Copyright © 2018 None. All rights reserved.
//

import UIKit
import CoreData

let kAPP_STYLE_COLOR = UIColor(displayP3Red: 255/255.0, green: 59/255.0, blue: 48/255.0, alpha: 1.0)
let kSETTING_UNIT_FORMAT_SYSTEM = "unit_format_system"
enum UnitFormat : String {
    case standard
    case metric
    case imperial
}

class SettingsVC: UIViewController {
    
    @IBOutlet weak var unitFormatSeg: UISegmentedControl!
    @IBOutlet weak var resetBtn: UIButton!
    let kButtonRadius:CGFloat = 10.0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let defaults = UserDefaults.standard
        if let value = defaults.value(forKey: kSETTING_UNIT_FORMAT_SYSTEM) as? Int {
            unitFormatSeg.selectedSegmentIndex = value
        }
        
        self.navigationController?.navigationBar.backgroundColor = kAPP_STYLE_COLOR
        
        resetBtn.layer.cornerRadius = kButtonRadius
    }
    
    
    @IBAction func unitFormatValueChanged(_ sender: Any) {
        
        let defaults = UserDefaults.standard
        defaults.set(unitFormatSeg.selectedSegmentIndex, forKey: kSETTING_UNIT_FORMAT_SYSTEM)
        
    }
    @IBAction func resetBookmark(_ sender: Any) {
        getPermissionClearBookmark()
    }
    
    func getPermissionClearBookmark() {
        let alertController = UIAlertController(title: "Clear Bookmark", message: "Are you sure?", preferredStyle: .alert)
        
        let action1 = UIAlertAction(title: "Reset", style: .default) { (action:UIAlertAction) in
            print("You've pressed add");
            
            // clear bookmark
            self.deleteAllWeatherStatus()
        }
        let action2 = UIAlertAction(title: "No", style: .cancel) { (action:UIAlertAction) in
            print("You've pressed cancel");
            // do nothing
        }
        
        alertController.addAction(action1)
        alertController.addAction(action2)
        self.present(alertController, animated: true, completion: nil)
    }
    
    // MARK: Core Data
    func deleteAllWeatherStatus() {
        
        // Create Managed Object
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        let managedObjectContext = appDelegate.persistentContainer.viewContext
        
        // Initialize Fetch Request
        let fetchRequest:NSFetchRequest<WeatherStatus> = WeatherStatus.fetchRequest()
        
        // execute
        do {
            let result = try managedObjectContext.fetch(fetchRequest)
            for obj:WeatherStatus in result {
                managedObjectContext.delete(obj)
            }
            
        } catch {
            let fetchError = error as NSError
            print(fetchError)
        }
        
        // save
        do {
            try managedObjectContext.save()
        } catch {
            let saveError = error as NSError
            print(saveError)
        }
    }
}

extension SettingsVC {
    var unit_format:String {
        let defaults = UserDefaults.standard
        
        if let value = defaults.value(forKey: kSETTING_UNIT_FORMAT_SYSTEM) as? Int {
            switch (value) {
            case UnitFormat.imperial.hashValue:
                return UnitFormat.imperial.rawValue
            case UnitFormat.metric.hashValue:
                return UnitFormat.metric.rawValue
            default:
                return UnitFormat.standard.rawValue
            }
        }
        return UnitFormat.standard.rawValue
    }
}
