//
//  HelpVC.swift
//  OpenWeather
//
//  Created by Alvin Luu on 8/6/18.
//  Copyright © 2018 None. All rights reserved.
//

import UIKit

class HelpVC: UIViewController {
    
    @IBOutlet weak var webView: UIWebView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        webView.loadHTMLString("<html><body><h1> Hello World!!! </h1></body></html>", baseURL: nil)
//        let nsurl = NSURLRequest(url: <#T##URL#>)
        webView.loadRequest(URLRequest(url: URL(fileURLWithPath: Bundle.main.path(forResource: "HTML_Help", ofType: "html")!)))
        
        self.navigationController?.navigationBar.backgroundColor = kAPP_STYLE_COLOR
    }
}
