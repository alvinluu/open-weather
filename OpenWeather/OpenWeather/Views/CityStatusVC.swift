//
//  CityStatusVC.swift
//  OpenWeather
//
//  Created by Alvin Luu on 8/4/18.
//  Copyright © 2018 None. All rights reserved.
//

import UIKit
import MapKit

class CityStatusVC: UIViewController {
    @IBOutlet weak var cityL: UILabel!
    @IBOutlet weak var temperatureL: UILabel!
    @IBOutlet weak var humidityL: UILabel!
    @IBOutlet weak var windSpeedL: UILabel!
    @IBOutlet weak var chanceOfRainL: UILabel!
    @IBOutlet weak var weatherDescL: UILabel!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var weatherStatusView: UIStackView!
    @IBOutlet weak var forecastCV: UICollectionView!
    
    
    var weatherStatus: WeatherStatus!
    var cityWeather: CityWeather!
    var cityForecast: [CityForecast]!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        cityForecast = [CityForecast]()
        
        initOutlets()
        
        self.navigationItem.setRightBarButton(UIBarButtonItem(barButtonSystemItem: .trash, target: self, action: #selector(deleteBookmark)), animated: true)
        self.navigationItem.title = "Weather"
        self.navigationController?.navigationBar.backgroundColor = kAPP_STYLE_COLOR
    }
    
    
    
    //MARK: - Navigation Controller
    
    func deleteBookmark() {
        
        //alert deletion
        let alertController = UIAlertController(title: "Remove Location", message: "Do you want to remove current location from Bookmark?", preferredStyle: .alert)
        
        let action1 = UIAlertAction(title: "Remove", style: .default) { (action:UIAlertAction) in
            print("You've pressed remove");
            // delete bookmark
            self.deleteWeatherStatus()
            
            // return to previous view
            self.navigationController?.popViewController(animated: true)
            
        }
        
        let action2 = UIAlertAction(title: "No", style: .cancel) { (action:UIAlertAction) in
            print("You've pressed cancel");
            // do nothing
            
            // dismiss alert
            
        }
        
        alertController.addAction(action1)
        alertController.addAction(action2)
        self.present(alertController, animated: true, completion: nil)
        
        
        
        
    }
    
    //MARK: - Setup View
    func initOutlets() {
        if weatherStatus != nil {
            DispatchQueue.global(qos: .userInitiated).async {
                
                self.HTTP_GET_Weather_Request()
                DispatchQueue.main.async {
                    self.weatherStatusView.isHidden = true
                    self.activityIndicator.isHidden = false
                }
                self.HTTP_GET_Forecast_Request()
            }
        }
    }
    
    func setupOutlets() {
        DispatchQueue.main.async {
            
            if let w = self.cityWeather {
                
                self.cityL.text = w.city
                self.temperatureL.text = w.temperatureText
                self.humidityL.text = w.humidityText
                self.windSpeedL.text = w.windSpeedText
                self.chanceOfRainL.text = w.chanceOfRainText
                self.weatherDescL.text = w.weatherDesc
            }
            
            self.weatherStatusView.isHidden = false
            self.activityIndicator.isHidden = true
        }
    }
    
    
    // MARK: - HTTP Request
    func HTTP_GET_Weather_Request(){
        
        // example url http://api.openweathermap.org/data/2.5/weather?lat=0&lon=0&appid=c6e381d8c7ff98f0fee43775817cf6ad&units=metric
        //        let Url = String(format: "http://api.openweathermap.org/data/2.5/weather?lat=0&lon=0&appid=c6e381d8c7ff98f0fee43775817cf6ad&units=metric")
        let Url = String(format: URL_FETCH_WEATHER)
        let parameterDictionary = ["id" : weatherStatus.id, "appid" : "c6e381d8c7ff98f0fee43775817cf6ad", "units" : SettingsVC().unit_format] as [String : Any]
        var components = URLComponents()
        components.queryItems = queryItems(dictionary: parameterDictionary)
        let parameterString = components.url?.description ?? ""
        
        
        guard let serviceUrl = URL(string: Url + parameterString) else { return }
        var request = URLRequest(url: serviceUrl)
        request.httpMethod = "GET"
        
        let session = URLSession.shared
        session.dataTask(with: request) { (data, response, error) in
            if let response = response {
                print(response)
            }
            if let data = data {
                do {
                    let json = try JSONSerialization.jsonObject(with: data, options: []) as! [String : Any]
                    print("json data")
                    print(json)
                    if self.cityWeather == nil {
                        self.cityWeather = CityWeather()
                    }
                    self.cityWeather.parse(json: json)
                    self.setupOutlets()
                } catch {
                    print(error)
                }
            }
            }.resume()
    }
    
    
    func HTTP_GET_Forecast_Request(){
        
        // example url http://api.openweathermap.org/data/2.5/weather?lat=0&lon=0&appid=c6e381d8c7ff98f0fee43775817cf6ad&units=metric
        //        let Url = String(format: "http://api.openweathermap.org/data/2.5/weather?lat=0&lon=0&appid=c6e381d8c7ff98f0fee43775817cf6ad&units=metric")
        
        
        let Url = String(format: URL_FETCH_FORCAST)
        let parameterDictionary = ["id" : weatherStatus.id, "appid" : "c6e381d8c7ff98f0fee43775817cf6ad", "units" : SettingsVC().unit_format] as [String : Any]
        var components = URLComponents()
        components.queryItems = queryItems(dictionary: parameterDictionary)
        let parameterString = components.url?.description ?? ""
        
        
        guard let serviceUrl = URL(string: Url + parameterString) else { return }
        var request = URLRequest(url: serviceUrl)
        request.httpMethod = "GET"
        
        let session = URLSession.shared
        session.dataTask(with: request) { (data, response, error) in
            if let response = response {
                print(response)
            }
            if let data = data {
                do {
                    let json = try JSONSerialization.jsonObject(with: data, options: []) as! [String:Any]
                    //                    print("json data")
                    //                    print(json)
                    
                    
                    if let list = json["list"] as? [[String:Any]] {
                        if self.cityForecast == nil {
                            self.cityForecast = [CityForecast]()
                        }
                        
                        for dict:[String:Any] in list {
                            
                            let forecast = CityForecast()
                            let currentTS = dict["dt"] as? Int64
                            var shouldAppend = false
                            
                            
                            if let preForecast = self.cityForecast.last {
                                
                                let aDayTS:Int64 = 24 * 3600
                                if let c = currentTS, c == (preForecast.timestamp + aDayTS) {
                                    shouldAppend = true
                                }
                            } else {
                                shouldAppend = true
                            }
                            
                            if shouldAppend {
                                forecast.timestamp = currentTS
                                forecast.weather.parse(json: dict)
                                self.cityForecast.append(forecast)
                            }
                            
                        }
                        
                        DispatchQueue.main.async {
                            self.forecastCV.reloadData()
                        }
                    }
                } catch {
                    print(error)
                }
            }
            }.resume()
    }
    
    func queryItems(dictionary: [String:Any]) -> [URLQueryItem] {
        return dictionary.map {
            // Swift 3
            URLQueryItem(name: $0, value: "\($1)")
            
            // Swift 4
            //            URLQueryItem(name: $0.0, value: $0.1)
        }
    }
    
    // MARK: Core Data
    
    func deleteWeatherStatus() {
        
        // Create Managed Object
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        let managedObjectContext = appDelegate.persistentContainer.viewContext
        
        // execute
        managedObjectContext.delete(weatherStatus)
        
        do {
            try managedObjectContext.save()
        } catch {
            let saveError = error as NSError
            print(saveError)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}

extension CityStatusVC: UICollectionViewDelegate, UICollectionViewDataSource
{
    //MARK: datasource
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        numberOfItemsInSection section: Int) -> Int {
        return cityForecast.count
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let reuseIdentifier = "cell"
        var cell:ForecastCell = ForecastCell()
        
        if let c = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier,
                                                      for: indexPath) as? ForecastCell
        {
            cell = c
        }
        
        // Configure the cell
        let forecast = cityForecast[indexPath.row]
        cell.temperature.text = forecast.weather.temperatureText
        cell.date.text = forecast.weather.datetimeText
        
        return cell
    }
}

