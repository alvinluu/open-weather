//
//  ForecastCell.swift
//  OpenWeather
//
//  Created by Alvin Luu on 8/6/18.
//  Copyright © 2018 None. All rights reserved.
//

import Foundation
import UIKit

class ForecastCell: UICollectionViewCell {
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var temperature: UILabel!
}
