//
//  WeatherStatus+CoreDataClass.swift
//  OpenWeather
//
//  Created by Alvin Luu on 8/5/18.
//  Copyright © 2018 None. All rights reserved.
//

import Foundation
import CoreData


public class WeatherStatus: NSManagedObject {
    
    func add(completion: ()->()) {
        print(self)
        
        do {
            try self.managedObjectContext?.save()
        } catch {
            print(error)
        }
    }
    
    func parse(json: [String:Any]) {
        if let main = json["main"] as? [String:Any] {
            if let temp = main["temp"] as? Float {
                self.temperature = Int16(round(temp))
            }
            if let humidity = main["humidity"] as? Int16 {
                self.humidity = humidity
            }
            
        }
        if let wind = json["wind"] as? [String:Any] {
            if let speed = wind["speed"] as? Float {
                self.windSpeed = Int16(round(speed))
            }
        }
        if let cityName = json["name"] as? String {
            self.name = cityName
        }
        if let id = json["id"] as? Int32 {
            self.id = id
        }
    }
    

    
}
