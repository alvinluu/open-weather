//
//  WeatherStatus+CoreDataProperties.swift
//  OpenWeather
//
//  Created by Alvin Luu on 8/5/18.
//  Copyright © 2018 None. All rights reserved.
//

import Foundation
import CoreData


extension WeatherStatus {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<WeatherStatus> {
        return NSFetchRequest<WeatherStatus>(entityName: "WeatherStatus")
    }

    @NSManaged public var chanceOfRain: Int16
    @NSManaged public var humidity: Int16
    @NSManaged public var id: Int32
    @NSManaged public var name: String?
    @NSManaged public var temperature: Int16
    @NSManaged public var windSpeed: Int16

}
